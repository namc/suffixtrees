package suffixtree;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by namrata on 5/27/15.
 */
public class Utils {

    public static String normalize(String in) {
        StringBuilder out = new StringBuilder();
        String l = in.toLowerCase();
        for (int i = 0; i < l.length(); ++i) {
            char c = l.charAt(i);
            if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9') {
                out.append(c);
            }
        }
        return out.toString();
    }

    public static Set<String> getSubstrings(String str) {
        Set<String> ret = new HashSet<String>();
        for (int len = 1; len <= str.length(); ++len) {
            for (int start = 0; start + len <= str.length(); ++start) {
                String itstr = str.substring(start, start + len);
                ret.add(itstr);
            }
        }
        return ret;
    }
}
